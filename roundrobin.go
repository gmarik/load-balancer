package main

import (
	"sync"
)

// RoundRobin implements simple Scheduler
type RoundRobin struct {
	Pool interface {
		Endpoints() []string
	}
	mu sync.RWMutex
	i  int
}

// Endpoints returns list of the endpoints sorted by priority.
// RoundRobin scheduler circulates endpoints.
func (rr *RoundRobin) Endpoints() []string {
	rr.mu.Lock()
	defer rr.mu.Unlock()

	var (
		es = rr.Pool.Endpoints()
		n  = len(es)
	)

	switch n {
	case 0, 1:
		return es
	default:
		ordered := make([]string, n)
		for i := range es {
			ordered[i] = es[(i+rr.i)%n]
		}
		rr.i = (rr.i + 1) % n
		return ordered
	}
}
