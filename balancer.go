package main

import (
	"net/http"
)

// LB is a load-balancer given the Transport and Scheduler
type LB struct {
	Transport http.RoundTripper
	Scheduler interface {
		Endpoints() []string
	}
}

// RoundTrip implements load balancing between Endpoints.
// Requests are made sequentially in the order Endpoints are provided by Scheduler.
// First non 500 response is returned, otherwise next endpoint is tried.
// Once Endpoints are exhausted 503 is returned.
func (lb *LB) RoundTrip(r *http.Request) (*http.Response, error) {
	for _, endpoint := range lb.Scheduler.Endpoints() {
		// direct to an endpoint
		r.URL.Host = endpoint
		r.URL.Scheme = "http"
		r.Close = false

		resp, err := lb.Transport.RoundTrip(r)
		if err != nil || resp.StatusCode == http.StatusInternalServerError {
			continue
		}
		return resp, nil
	}

	return &http.Response{StatusCode: http.StatusServiceUnavailable}, nil
}
