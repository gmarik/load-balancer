## Submission

- sketched main structure and components
- main focus on single resposibility and composition
- i could continue perfect the solution but i'd rather recive feedback early
- requires work on correctnes(more tests) and performance

## Original Description
### Go Load Balancer

The purpose of this exercise is to write a simple HTTP load balancer that is capable of managing multiple server instances in a pool and balancing the incoming requests across those instances.
Vimeo has provided a server instance with the following characteristics:
• starts on a user-defined port, defaulting to 8300
• periodically changes its instance status to mimic an intermittently failing
server

#### Instance Status

The Vimeo server provides a status endpoint accessible at /_health which returns one of the following JSON responses:
• `{"state": "healthy", message: "..."}` indicating that the server is in service and should continue to receive traffic
• `{"state": "degraded", message: "..."}` indicating the server is out of service and should no longer receive traffic
Note that an instance may change state in either direction (from healthy to degraded and from degraded to healthy).

#### Balancing Traffic

- The load balancer’s job is to take incoming traffic and proxy it to one of the available healthy instances.
- If none of the instances are healthy, the load balancer should return an HTTP 503 response code.
- Simultaneously, the load balancer should poll all the registered instances at an interval using the `/_health` endpoint to update their status in the load balancer. Only instances that identify themselves as healthy should receive traffic.
- If a server instance returns an HTTP 500 response code, the request should be retried against one of the other healthy instances.
- It is understood that the state of an instance may change in between polling intervals, resulting in the possibility of an degraded instance receiving traffic before the next polling event has an opportunity to update the load balancer’s internal state.


#### Accepting Input

The load balancer will need to understand where the various server instances are running. This can be done via command line arguments or configuration files.

##### Sample Invocation
```shell
$ ./challenge-darwin server -p 9000
$ ./challenge-darwin server -p 9001
$ ./load-balancer -b http://localhost:9000 -b http://localhost:9001 -p 8000
```

##### Discussion
Your solution should have detailed documentation and a full explanation of your solution. Feel free to discuss bottlenecks, benchmarks, and performance considerations in your submission. Vimeo strongly values communication skills, so feel free to give insight into what you were thinking while implementing your solution.

##### Submission

Please package and email your solution (or make it available via a private link) to Vimeo recruiting. Please do not post or share it publicly (GitHub, etc.).
