package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"time"
)

// Proxy implements http.Handler delegating requests to the Transport
type Proxy struct {
	Transport http.RoundTripper
}

// ServeHTTP implements http.Handler interface so
// http.Server could be used as a proxy front-end
func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()
	resp, err := p.Transport.RoundTrip(r.WithContext(ctx))
	if err != nil {
		log.Println("requester:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(resp.StatusCode)
	for k, v := range resp.Header {
		w.Header()[k] = v
	}
	if resp.Body != nil {
		io.Copy(w, resp.Body)
		resp.Body.Close()
	}
}
