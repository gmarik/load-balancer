package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	healthCheckInteval := flag.Duration("health-check", 2*time.Second, "health-check interval")
	port := flag.Int("p", 9000, "port to listen on")
	verbose := flag.Bool("v", false, "verbose output")
	var endpoints staticPool
	flag.Var(&endpoints, "b", "balanced endpoint; multiple entries are allowed")
	flag.Parse()

	if len(endpoints) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		ctx, cancel = context.WithCancel(context.TODO())
		wg          waitGroup
	)

	//
	// common HTTP transport
	//
	var transport http.RoundTripper = &http.Transport{
		IdleConnTimeout:       5 * time.Second,
		ResponseHeaderTimeout: 5 * time.Second,
	}

	//
	// Logger
	//
	if *verbose {
		var stderr = log.New(os.Stderr, "", log.LstdFlags)
		transport = transportFunc(logger(stderr, transport))
	}

	//
	// Health
	//
	var healthyPool = &HealthyPool{
		Checker: &Checker{
			Probe:     "/_health",
			Transport: transport,
		},
		Pool:      StaticPool(endpoints),
		Transport: transport,
	}

	// health-checking process
	wg.Start(func() {
		healthyPool.Refresh(ctx, *healthCheckInteval)
	})

	//
	// HTTP Server and Handler
	//
	var server = http.Server{
		Addr: fmt.Sprint(":", *port),
		Handler: &Proxy{
			Transport: &LB{
				Scheduler: &RoundRobin{Pool: healthyPool},
				Transport: &HealthyTransport{
					Transport: transport,
					Pool:      healthyPool,
				},
			},
		},
	}

	wg.Start(func() {
		if err := server.ListenAndServe(); err != nil {
			cancel()
			log.Printf("server=stopping err=%q\n", err)
		}
	})
	wg.Start(func() {
		<-ctx.Done()
		server.Shutdown(ctx)
	})

	//
	// Signals
	//
	wg.Start(func() {
		sigc := make(chan os.Signal)
		signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)
		select {
		case sig := <-sigc:
			log.Printf("received sig=%v", sig)
			cancel()
		case <-ctx.Done():
			return
		}
	})

	log.Printf("started port=%d", *port)
	wg.Wait()
	log.Printf("stopped")
}

// helper wrapper to ensure clean goroutine shutdown
type waitGroup struct {
	wg sync.WaitGroup
}

func (wg *waitGroup) Start(worker func()) {
	wg.wg.Add(1)
	go func() {
		worker()
		wg.wg.Done()
	}()
}

func (wg *waitGroup) Wait() {
	wg.wg.Wait()
}

// helper middleware for debug purposes
func logger(l *log.Logger, next http.RoundTripper) transportFunc {
	return func(r *http.Request) (*http.Response, error) {
		resp, err := next.RoundTrip(r)
		if err != nil {
			l.Printf("endpoint=%q err=%q", r.URL.Host, err)
		} else {
			if resp.StatusCode >= 500 {
				l.Printf("endpoint=%q status=%d", r.URL.Host, resp.StatusCode)
			}
		}
		return resp, err
	}
}

type transportFunc func(*http.Request) (*http.Response, error)

func (f transportFunc) RoundTrip(r *http.Request) (*http.Response, error) {
	return f(r)
}

//
// flag.Var
//
type staticPool []string

func (sp *staticPool) Set(value string) error {
	*sp = append(*sp, value)
	return nil
}

func (sp *staticPool) String() string {
	return fmt.Sprint(*sp)
}
