package main

import (
	"testing"
)

func Test_RoundRobin(t *testing.T) {
	s := RoundRobin{
		Pool: StaticPool([]string{"1", "2", "3"}),
	}

	tcases := [][]string{
		[]string{"1", "2", "3"},
		[]string{"2", "3", "1"},
		[]string{"3", "1", "2"},
		[]string{"1", "2", "3"},
		[]string{"2", "3", "1"},
	}

	for i := range tcases {
		if exp, got := tcases[i], s.Endpoints(); !equal(exp, got) {
			t.Fatalf("exp=%v got=%v", exp, got)
		}
	}
}

func equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
