package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Benchmark_Proxy(b *testing.B) {

	backend := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		n := rand.Intn(100)
		healthy := n <= 10
		switch {
		case r.URL.Path == "/_health":
			status := "healthy"
			if !healthy {
				status = "degraded"
			}
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, `{"state":%q,"message":%q}`, status, status)
		default:
			status := http.StatusOK
			if !healthy {
				status = http.StatusServiceUnavailable
			}
			w.WriteHeader(status)
			fmt.Fprintf(w, `{"val":%d}`, n)
		}
	})

	backend1 := httptest.NewServer(backend)
	backend2 := httptest.NewServer(backend)

	transport := &http.Transport{}
	healthyPool := &HealthyPool{
		Checker: &Checker{
			Probe:     "/_health",
			Transport: transport,
		},
		Pool:      StaticPool([]string{backend1.URL[7:], backend2.URL[7:]}),
		Transport: transport,
	}

	proxy := &Proxy{
		Transport: &LB{
			Scheduler: &RoundRobin{Pool: healthyPool},
			Transport: &HealthyTransport{
				Transport: transport,
				Pool:      healthyPool,
			},
		},
	}

	srv := httptest.NewServer(proxy)
	defer srv.Close()

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		resp, err := http.Get(srv.URL)
		if err != nil {
			b.Fatal(err)
		}
		resp.Body.Close()
	}
}
