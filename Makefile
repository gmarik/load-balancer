.PHONY: test
test:
	go test ./

.PHONY: bench
bench:
	go test -bench . ./

.PHONY: build
build:
	go build -o load-balancer ./
