package main

// Pool defines a pool of endpoints
type Pool interface {
	Endpoints() []string
}

// StaticPool is a static endpoints list
type StaticPool []string

// Endpoints conforms to Pool interface
func (sp StaticPool) Endpoints() []string {
	return []string(sp)
}
