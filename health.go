package main

import (
	"context"
	"encoding/json"
	"net/http"
	"sync"
	"time"
)

// CheckTimeout sets the maximum time a checker waits from an endpoint to respond.
// Otherwise the endpoint is considered unhealthy.
const CheckTimeout = 50 * time.Millisecond

// HealthyTransport is a transport wrapper
// that keeps track of endpoint responses and updates the pool.
type HealthyTransport struct {
	Transport http.RoundTripper
	Pool      interface {
		Unhealthy(e string)
	}
}

// RoundTrip makes a request and checks for the response errors.
// In case of any errors the endpoint is marked as unhealthy.
func (ht *HealthyTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	resp, err := ht.Transport.RoundTrip(r)
	if err != nil || resp.StatusCode == http.StatusInternalServerError {
		ht.Pool.Unhealthy(r.URL.Host)
	}

	return resp, err
}

// HealthyPool is a Pool of endpoints
// where endpoints are regularly health-checked.
type HealthyPool struct {
	Transport http.RoundTripper
	Pool      Pool
	Checker   interface {
		Check(context.Context, []string) []string
	}

	mu        sync.RWMutex
	endpoints []string
}

// Unhealthy provides a way to flag an endpoint as "unhealthy",
// which effectively eliminates it from the pool until a regula health-check decides otherwise.
func (hp *HealthyPool) Unhealthy(e string) {
	hp.mu.Lock()
	defer hp.mu.Unlock()

	indexOf := func(e string) int {
		for i, ee := range hp.endpoints {
			if ee == e {
				return i
			}
		}
		return -1
	}

	i := indexOf(e)
	n := len(hp.endpoints)
	switch {
	case i < 0:
		return
	case n == 0:
		return
	case i == n-1:
		hp.endpoints = hp.endpoints[:n-1]
		return
	default:
		hp.endpoints[i] = hp.endpoints[n-1]
		hp.endpoints = hp.endpoints[:n-1]
	}

	return
}

// Endpoints returns a list of healthy endpoints.
func (hp *HealthyPool) Endpoints() []string {
	hp.mu.RLock()
	defer hp.mu.RUnlock()
	if len(hp.endpoints) == 0 {
		return nil
	}
	endpoints := make([]string, len(hp.endpoints))
	copy(endpoints, hp.endpoints)

	return endpoints
}

// Refresh periodically health-checks the endpoints
func (hp *HealthyPool) Refresh(ctx context.Context, refreshInterval time.Duration) error {
	var (
		ticker = time.NewTicker(refreshInterval)
	)
	// refresh upon start
	hp.refresh(ctx)
	for {
		select {
		case <-ctx.Done():
			return nil
		case <-ticker.C:
			hp.refresh(ctx)
		}
	}
}

func (hp *HealthyPool) refresh(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, CheckTimeout)
	defer cancel()
	endpoints := hp.Checker.Check(ctx, hp.Pool.Endpoints())
	hp.mu.Lock()
	hp.endpoints = endpoints
	hp.mu.Unlock()
}

// Checker is a helper type that implements specifics of health-check-ing endpoints.
type Checker struct {
	Transport http.RoundTripper
	Probe     string
}

// Check implement parallel health-check of the provided endpoints
func (ch *Checker) Check(ctx context.Context, endpoints []string) []string {
	if len(endpoints) == 0 {
		return nil
	}
	var (
		checkWg sync.WaitGroup
		healty  = make(chan string, len(endpoints))
	)
	for _, e := range endpoints {
		var e = e
		checkWg.Add(1)
		go func(e string) {
			defer checkWg.Done()
			status, err := ch.fetchStatus(ctx, e)
			if err != nil || status == "unhealhty" {
				return
			}
			healty <- e
		}(e)
	}
	checkWg.Wait()
	close(healty)
	endpoints = make([]string, 0, len(endpoints))
	for e := range healty {
		endpoints = append(endpoints, e)
	}

	return endpoints
}

func (ch *Checker) fetchStatus(ctx context.Context, hostPort string) (string, error) {
	r, err := http.NewRequest("GET", "http://"+hostPort+ch.Probe, nil)
	if err != nil {
		return "", err
	}
	ctx, cancel := context.WithTimeout(ctx, CheckTimeout)
	defer cancel()
	resp, err := ch.Transport.RoundTrip(r.WithContext(ctx))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// {"state":"healthy","message":"healthy"}
	// {"state":"degraded","message":"degraded"}
	var state struct {
		State   string `json:"state"`
		Message string `json:"message"`
	}

	var dec = json.NewDecoder(resp.Body)
	if err := dec.Decode(&state); err != nil {
		return "", err
	}

	return state.State, nil
}
